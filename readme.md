# Test project to study JWT authorization in Node.js
It uses Passport Middleware to make JWT authorization via Rest API

## Configuration
Create the .env file using the structure from .env-example

## Running
``` bash
git clone https://gitlab.com/nazar_bashinskiy/studying-jwt-authorization
cd studying-jwt-authorization
npm install
npm run start
```