const dotenv = require('dotenv');
const express = require('express');
const passport = require('passport');

const routes = require('./routes');
const passportConfig = require('./passport.config');

dotenv.config();

const app = express();

app.use(express.json());
app.use(passport.initialize());

passportConfig(passport);
routes(app);

app.listen(process.env.PORT, () => console.log(`App started at port ${process.env.PORT}`));