const LocalStrategy = require('passport-local');
const jwtStrategy = require('passport-jwt');

module.exports = function(passport){
    passport.serializeUser((user, done) => {
        done(null, user);
    });
      
    passport.deserializeUser((user, done) => {
       done(null, user);
    });

    passport.use(new LocalStrategy((username, password, done) => {
        const user = {
            id: 12,
            login: username,
            password: '1234567'
        }

        if (!user)
            return done(null, false, { message: 'Incorrect username.' });
        
        else if (user.password != password)
            return done(null, false, { message: 'Invalid username.' });
        
        else 
            return done(null, user);
    }));

    passport.use(new jwtStrategy.Strategy({
        jwtFromRequest: jwtStrategy.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.jwtSecret
    }, (jwtPayload, done) => {
        const user = {
            id: 12,
            login: jwtPayload.login,
            password: '1234567'
        }

        delete user.password;
        
        if (!user)
            return done(null, false, { message: 'Incorrect username.' });

        else 
            return done(null, user);
    }));
}