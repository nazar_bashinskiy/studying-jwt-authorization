const authRoute = require('./auth');
const blogRoute = require('./blog');

module.exports = function(app){
    authRoute(app);
    blogRoute(app);
}