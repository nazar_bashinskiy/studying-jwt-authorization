const passport = require('passport');

module.exports = function(app){
    app.get('/blog', passport.authenticate('jwt'), (req, res) => 
        res.send(req.user));
}