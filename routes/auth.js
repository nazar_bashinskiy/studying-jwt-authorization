const passport = require('passport');
const jwt = require('jsonwebtoken');

module.exports = function(app){
    app.post('/auth', passport.authenticate('local'), (req, res) => {
        delete req.user.password;
        res.send(jwt.sign(req.user, process.env.jwtSecret))
    });
}